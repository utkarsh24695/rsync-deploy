import os
import shutil
import base64
from random import randint
from io import StringIO
from pathlib import Path

import pytest
import boto3
import requests
import subprocess

from bitbucket_pipes_toolkit.test import PipeTestCase
from bitbucket_pipes_toolkit.helpers import get_variable


class TargetHostPublicIpNotFound(Exception):
    pass


DIRNAME = os.path.dirname(__file__)
BASE_LOCAL_DIR = os.path.join(DIRNAME, 'tmp')
LOCAL_DIR = os.path.join(BASE_LOCAL_DIR, 'rsync')
TEST_SSH_CONFIG_DIR = "/opt/atlassian/pipelines/agent/build/test_ssh"

AWS_DEFAULT_REGION = 'us-east-1'
STACK_NAME = f"bbci-pipes-test-infrastructure-ec2-rsync-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
REMOTE_PATH = "/usr/share/nginx/html/rsync"


def get_target_host_ip(stack_name):
    client = boto3.client('ec2', region_name=AWS_DEFAULT_REGION)
    instances = client.describe_instances(
        Filters=[
            {'Name': 'tag:aws:cloudformation:stack-name', 'Values': [stack_name]},
            {'Name': 'instance-state-name', 'Values': ['running']},
        ]
    )
    try:
        return instances['Reservations'][0]['Instances'][0]['PublicIpAddress']
    except IndexError:
        raise TargetHostPublicIpNotFound(stack_name)


def setup_test_ssh_dir(target_host_ip):
    os.makedirs(TEST_SSH_CONFIG_DIR, exist_ok=True)

    # Copy test infrastructure key to test directory
    test_ssh_key = os.getenv('SSH_TEST_KEY')
    with open(f"{TEST_SSH_CONFIG_DIR}/id_rsa_tmp", "w") as f:
        key_content = base64.b64decode(test_ssh_key)
        f.write(key_content.decode())

    # Add auto generated EC2 instance known_hosts entries.
    with open(f"{TEST_SSH_CONFIG_DIR}/known_hosts", 'ab+') as f:
        resp = subprocess.run([
            'ssh-keyscan',
            '-t',
            'rsa',
            target_host_ip,
        ], stdout=subprocess.PIPE)
        f.write(resp.stdout)


def setup_one_test_file(dirname, filename):
    rm_files(BASE_LOCAL_DIR)

    try:
        os.makedirs(os.path.join(dirname))
    except OSError:
        print(f'Creation of the directory {dirname} failed')
    else:
        print(f'Successfully created the directory {dirname}')

    file_path = f'{dirname}/{filename}'
    Path(file_path).touch()


def setup_file(dirname, msg):
    """
    Setup files to sync with remote server
    """
    result_files = []

    random_number = randint(0, 100)

    # clean up & create paths
    rm_files(BASE_LOCAL_DIR)

    try:
        os.makedirs(os.path.join(dirname, 'subdir'))
    except OSError:
        print(f'Creation of the directory {dirname} failed')
    else:
        print(f'Successfully created the directory {dirname}')

    for i in range(1, 3+1):
        nested_dir = 'subdir/' if i % 3 == 0 else ''
        path = f'{dirname}/{nested_dir}deployment-{random_number}-{i}.txt'
        result_files.append(f'{nested_dir}deployment-{random_number}-{i}.txt')

        with open(path, 'w') as f:
            f.write(f'Pipelines is awesome {random_number}! {msg}')

    return result_files


def rm_files(path):
    """
    Clean up test generated files
    """
    try:
        shutil.rmtree(path, ignore_errors=True)
    except OSError:
        print(f'Deletion of the directory {path} failed')
    else:
        print(f'Successfully deleted the directory {path}')


class RsyncDeployTestCase(PipeTestCase):
    # Directory that will be used to read from ssh keys inside docker container
    ssh_config_dir = '/opt/atlassian/pipelines/agent/ssh'
    ssh_key_file = 'id_rsa_tmp'

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # get Variables
        cls.USER = 'ec2-user'
        cls.SERVER_IP = get_target_host_ip(STACK_NAME)
        cls.LOCAL_PATH = 'test/tmp'
        cls.REMOTE_PATH = REMOTE_PATH
        cls.DEBUG = get_variable('DEBUG')
        cls.EXTRA_ARGS = get_variable('EXTRA_ARGS')

        setup_test_ssh_dir(cls.SERVER_IP)

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.api_client = self.docker_client.api

        # redirect ssh output
        self.output = StringIO()

    def tearDown(self):
        # clean test generated files
        rm_files(BASE_LOCAL_DIR)

    def test_no_parameters(self):
        result = self.run_container()
        self.assertIn('SSH_USER variable missing', result)

    def test_default_success(self):
        # generate new files
        message = 'default_success'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_false_delete_after_file_remain(self):
        # generate file
        filename = "test_file.txt"
        setup_one_test_file(dirname=LOCAL_DIR, filename=filename)
        # run container
        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'DELETE_FLAG': 'false',
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })
        self.assertIn(f'Deployment finished', result)

        # generate new files
        message = 'without_delete_after_file_remain'
        generated_files = setup_file(LOCAL_DIR, message)
        
        # run container
        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'DELETE_FLAG': 'false',
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })
        self.assertIn(f'Deployment finished', result)

        # check old file remains
        response = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{filename}")
        self.assertEqual(response.status_code, 200)

    def test_success_arg_ssh_key(self):
        # generate new files
        message = 'success_arg_ssh_key'
        generated_files = setup_file(LOCAL_DIR, message)

        with open(os.path.join(TEST_SSH_CONFIG_DIR, self.ssh_key_file),
                  'rb') as identity_file:
            identity_content = identity_file.read()

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'SSH_KEY': base64.b64encode(identity_content),
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_extra_args(self):
        # generate new files
        message = 'success_extra_args'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'EXTRA_ARGS': '--exclude=*2.txt',
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        # exclude second filename
        # according to EXTRA_ARGS '--exclude=*2.txt'
        for i in [0, 2]:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_files[i]}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_extra_args_array(self):
        # generate new files
        message = 'success_extra_args'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'EXTRA_ARGS_COUNT': 1,
                'EXTRA_ARGS_0': '--rsync-path="rsync"',
                'DDBUG': 'true'
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_arg_ssh_port_default(self):
        # generate new files
        message = 'default_success'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'SSH_PORT': '8888',
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_arg_ssh_port_key_algorithm(self):
        # generate new files
        message = 'default_success'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'SSH_PORT': '8888',
                'SSH_ARGS': '-o HostKeyAlgorithms=ssh-rsa'
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_arg_ssh_no_port(self):
        # generate new files
        message = 'default_success'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
                'SSH_ARGS': '-o HostKeyAlgorithms=ssh-rsa'
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )

    def test_success_arg_no_ssh_no_port(self):
        # generate new files
        message = 'default_success'
        generated_files = setup_file(LOCAL_DIR, message)

        result = self.run_container(
            environment={
                'USER': self.USER,
                'SERVER': self.SERVER_IP,
                'REMOTE_PATH': self.REMOTE_PATH,
                'LOCAL_PATH': self.LOCAL_PATH,
            },
            volumes={
                TEST_SSH_CONFIG_DIR: {'bind': self.ssh_config_dir, 'mode': 'rw'}
            })

        self.assertIn(f'Deployment finished', result)

        generated_number = generated_files[0].split('-')[1]

        for generated_file in generated_files:
            resp = requests.get(f"http://{self.SERVER_IP}/rsync/tmp/rsync/{generated_file}")

            self.assertIn(
                f'Pipelines is awesome {generated_number}! {message}',
                resp.text
            )
