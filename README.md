# Bitbucket Pipelines Pipe: RSYNC deploy

Deploy your code to any server over ssh using rsync. By default it will upload the whole directory to the target.
Rsync is famous for its delta-transfer algorithm, which reduces the amount of data sent over the network by sending only the differences between the source files and the existing files in the destination.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:           
    
```yaml
- pipe: atlassian/rsync-deploy:0.7.1
  variables:
    USER: '<string>'
    SERVER: '<string>'
    REMOTE_PATH: '<string>'
    LOCAL_PATH: '<string>'
    # SSH_KEY: '<string>' # Optional.
    # SSH_PORT: '<string>' # Optional.
    # SSH_ARGS: '<string>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
    # DELETE_FLAG: '<boolean>' # Optional.
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| SERVER (*)            | The remote host to transfer the files to. |
| USER (*)              | The user on the remote host to connect as. |
| REMOTE_PATH (*)       | The remote path to deploy files to. |
| LOCAL_PATH (*)        | The local path to file or folder to be deployed. |
| SSH_KEY               | An alternate SSH_KEY to use instead of the key configured in the Bitbucket Pipelines admin screens (which is used by default). This should be encoded as per the instructions given in the docs for [using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys) |
| SSH_PORT              | An alternative SSH port number for your rsync connection. By default it works with SSH_ARGS `-o StrictHostKeyChecking=no`. You can also pass your own SSH_ARGS.|
| SSH_ARGS              | SSH extra arguments when using rsync.  |
| EXTRA_ARGS            | Additional arguments passed to the rsync command (see [RSYNC docs](http://manpages.ubuntu.com/manpages/trusty/en/man1/rsync.1.html) for more details). |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
| DELETE_FLAG           | Use `--delete-after` option, which requests that the file-deletions on the receiving side be done after the transfer has completed. Default: `true`.

_(*) = required variable._

## Details

RSYNC copies files and directories between your local system to another server. It uses SSL for data transfer, which uses the same authentication
and provides the same security as SSH. It may also use many other features of SSH, such as compression.

Rsync finds files that need to be transferred using a "quick check" algorithm (by default) that looks for files that have changed in size or in last-modified time. Any changes in the other preserved attributes (as requested by options) are made on the destination file directly when the quick check indicates that the file’s data does not need to be updated.

More details: [rsync man page](http://manpages.ubuntu.com/manpages/trusty/en/man1/rsync.1.html)

By default, the RSYNC pipe will automatically use your configured SSH key and known_hosts file configured from the [Bitbucket Pipelines
administration pages](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html). You can pass
the `SSH_KEY` parameter to use an alternate SSH key as per the
instructions in the docs for
[using multiple ssh keys](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-multiple_keys)

## Prerequisites
* If you want to use the default behaviour for using the configured SSH key and known hosts file, you must have configured 
  the SSH private key and known_hosts to be used for the RSYNC pipe in your Pipelines settings
  (see [docs](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html))
 
## Examples

### Basic example:
    
```yaml
script:
  - pipe: atlassian/rsync-deploy:0.7.1
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build'
```

### Advanced examples:
Example with an alternative SSH port and extra debugging. Rsync remote shell will be executed with the default SSH_ARGS value `-o StrictHostKeyChecking=no`.
```yaml
script:
  - pipe: atlassian/rsync-deploy:0.7.1
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build'
      DEBUG: 'true'
      SSH_PORT: '8022'
```

Example with alternative port and another key checking algorithm.
```yaml
script:
  - pipe: atlassian/rsync-deploy:0.7.1
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build'
      DEBUG: 'true'
      SSH_PORT: '8022'
      SSH_ARGS: '-o HostKeyAlgorithms=ssh-rsa'
```

Example with custom SSH_ARGS.
```yaml
script:
  - pipe: atlassian/rsync-deploy:0.7.1
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build'
      DEBUG: 'true'
      SSH_ARGS: '-o HostKeyAlgorithms=ssh-rsa'
```

Example with an EXTRA_ARGS to exclude all files with '.txt' extension from deploy.
```yaml
script:
  - pipe: atlassian/rsync-deploy:0.7.1
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build'
      DEBUG: 'true'
      EXTRA_ARGS: '--exclude=*.txt'
```

Example with alternate SSH key.
    
```yaml
script:
  - pipe: atlassian/rsync-deploy:0.7.1
    variables:
      USER: 'ec2-user'
      SERVER: '127.0.0.1'
      REMOTE_PATH: '/var/www/build/'
      LOCAL_PATH: 'build'
      SSH_KEY: $MY_SSH_KEY
      DEBUG: 'true'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,rsync
